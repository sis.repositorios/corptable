class corpTables {

    constructor( id, color ){
        this.id     = id
        this.color  = color
    }

    draw( data ){

        let table = document.getElementById( this.id );
        data.forEach( element => {
            
            let tbodyRef = document.getElementById( this.id ).getElementsByTagName('tbody')[0];
            let newRow = tbodyRef.insertRow();

            element.forEach( atom => {
                let newCell = newRow.insertCell();
                let newText = document.createTextNode( atom );
                newCell.appendChild(newText);
            });

        });

    }

    clear(){
        let table = document.getElementById( this.id );
        let rows  = table.getElementsByTagName('tr');
        let count = rows.length;

        for( let x=count-1; x>0; x--){
            rows[x].innerHTML ='';
        }
    }

    sort( column ){

    }

    find( string ){

    }
}
